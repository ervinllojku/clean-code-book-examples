LEft here - There is great power in detail, yet there is something humble and profound about this
  approach to life, as we might stereotypically expect from any approach that claims Japanese
  roots.