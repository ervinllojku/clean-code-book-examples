class GuessStatisticsMessage {
    private String number;
    private String verb;
    private String pluralModifier;

    private String make(char candidate, int count)
    {
        createPluralDependentMessageParts(count);
        String guessMessage = String.format(
            "There %s %s %s %s", verb, number, candidate, pluralModifier
            );
    }

    public void createPluralDependentMessageParts(int count) {
        if (count == 0) {
            thereAreNoLetters();
        } else if (count == 1) {
            thereIsOneLetter();
        } else {
            thereAreManyLetters(count);
        }
    }

    public void thereAreNoLetters() {
        verb = "are";
        number = "no";
        pluralModifier = "s";
    }

    public void thereIsOneLetter(count) {
        verb = "is";
        number = "1";
        pluralModifier = "";
    }

    public void thereAreManyLetters(int count) {
        number = Integer.toString(number);
        verb = "are";
        pluralModifier = "s";
    }

}